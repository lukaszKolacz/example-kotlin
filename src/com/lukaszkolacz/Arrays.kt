package com.lukaszkolacz

val tablica : Array<Int> = arrayOf(1,2,3,4)
val tablica1 = arrayOf(1,2,3,4) //skompy zapis ale prawidlowy
val tablicaBool = booleanArrayOf(true,true,false)
//println(tablicaBool[2])

var tablicaLambdy = Array(10, {i -> i*i }) //0, 1, 4, 9, ... 81
//println(tablicaLambdy[5])

var teksts = "Hej!"
//println(teksts[1])

val a = arrayOf("string", 10)
//        val a1 = Array<Any> = tablica  Nieprawidłowy zapis
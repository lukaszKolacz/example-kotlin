package com.lukaszkolacz

//        Typy całkowitoliczbowe:
val osiembitow : Byte = 10
val szesnascieBitow : Short = Short.MAX_VALUE
val trzydziesciDwaBity : Int = Int.MAX_VALUE
val szescdziesiatCzteryBity : Long = Long.MAX_VALUE
//        Char Nie przechowuje liczb


//        Typy zmiennoprzecinkowe:
val trzydziesciDwa : Float = Float.MAX_VALUE
val szescdziesiatCztery : Double = Double.MAX_VALUE
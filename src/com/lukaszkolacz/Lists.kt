package com.lukaszkolacz

val tablica12 = arrayOf(1,2,3)            //w javie: int[] tablica = {1,2,3}

//        mutable/inmutable

val listInmutable : List<Int> = arrayListOf(1,2,3) //tylko do odczytu
val listMutable : List<Int> = mutableListOf(1,2,3) // do odczytu i zapisu (nie ma problemu z kompilacja po zmianie elementow lub dodaniu elementow)
val listMutable1 = mutableListOf(1,2,3)
//listMutable1.add(2)